public class arrayHelper {
    public static void main(String[] args){
        int zahlen[] = {1, 2, 3, 4, 5, 6};

		// Aufgabe 1
		String ergebnisAufgabe1 = convertArrayToString(zahlen);
		System.out.println(ergebnisAufgabe1);
		
		// Aufgabe 2
		int[] ergebnisAufgabe2 = zahlenUmdrehen(zahlen);
		System.out.println(java.util.Arrays.toString(ergebnisAufgabe2));
		
		// Aufgabe 3
		int[] ergebnisAufgabe3 = zahlenUmdrehenNeu(zahlen);
		System.out.println(java.util.Arrays.toString(ergebnisAufgabe3));
		
		// Aufgabe 4
		double[][] ergebnisAufgabe4 = temperaturTabelle(6);
    }



// Aufgabe 1
    public static String convertArrayToString(int[] zahlen){
        String text = "";

        for(int i = 0; i < zahlen.length; i++){
            text = text + (zahlen[i]); 
            if((i+1) < zahlen.length){
                text = text + (", ");
            }
        }
        return text;
    } 

// Aufgabe 2
    public static int[] zahlenUmdrehen(int[] zahlen){
        int speicher; 
        int laenge = zahlen.length - 1;
        int haelfte = zahlen.length/2;

        for(int i = 0; i <= haelfte; i++){
            speicher = zahlen[laenge];              //Zwischenspeicherung der hinteren Werte
            zahlen[laenge] = zahlen[i];             //Ersetzen der hinteren Werte, mit den vorederen Werten
            zahlen[i] = speicher;                   //Ersetzen der vorderen Werte, mit dem Zwischenspeicher
            laenge--;
        }
        return zahlen;
    }

    // Aufgabe 3
	public static int[] zahlenUmdrehenNeu(int[] zahlen) {
		int[] umgekehrt = new int[zahlen.length];
		byte x = 0;

		for (int i = zahlen.length - 1; i >= 0; i--) {
			umgekehrt[x] = zahlen[i];
			x += 1;
		}
		return umgekehrt;
	}

	// Aufgabe 4
	public static double[][] temperaturTabelle(int berechnung) {
		// 1. [] Fahrenheit / Celsius
		// 2. [] Werte
		// => [0][0] = Fahrenheit/0.0
		// => [1][0] = Celsius/-17.77
		double[][] tabelle = new double[2][berechnung];
		tabelle[0][0] = 0.00;

		for (int i = 0; i < berechnung; i++) {
			if (i > 0) {
				tabelle[0][i] = tabelle[0][i - 1] + 10.0;
			}
			tabelle[1][i] = (5.0 / 9.0) * (tabelle[0][i] - 32);
		}

		tabelleErstellen(tabelle, berechnung);
		return tabelle;
	}