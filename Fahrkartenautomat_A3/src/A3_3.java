import java.util.Scanner;

public class A3_3 
{	
	public static void main(String[] args) {
	Scanner scan = new Scanner(System.in);
	double zuZahlenderBetrag;
	double rueckgabebetrag;
	double anzahl;
	String frage = "j";
	
	while (frage.equals("j")) {
		anzahl = ticketAnzahl();
		zuZahlenderBetrag = fahrkartenbestellungErfassen(anzahl);
		rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben(anzahl);
		rueckgeldAusgeben(rueckgabebetrag, anzahl);
		System.out.println("Wollen Sie einen weiteren Kauf durchf�hren?");
		frage = scan.next();
		System.out.println();
	}
	}

//-----------------------------------------------------------------------------------------
// Code der die Anzahl der Tickets z�hlt und diese dann ausgibt. Wird ben�tigt, da ich in verschiedenen Methoden mit der Ticket Anzahl arbeite.
		
	public static double ticketAnzahl() {
	Scanner tastatur = new Scanner(System.in);
	double anzahl;
	
	System.out.println("Wie viele Tickets wollen Sie kaufen?");
	anzahl = tastatur.nextDouble();
	
	return anzahl;
	}
	
//-----------------------------------------------------------------------------------------
// Code der die Anzahl und den Preis von Tickets multipliziert und als Wert ausgibt.
	
	public static double fahrkartenbestellungErfassen(double x) {
	Scanner tastatur = new Scanner(System.in);
	double preis;
	
	System.out.println("\nWie viele kostet ein Ticket?");
	preis = tastatur.nextDouble();
	return(x * preis);
	}
	
//-----------------------------------------------------------------------------------------
// Code der sich um das Bezahlen des Tickets k�mmert und berechnet ob und wieviel R�ckgeld dem Kunden zusteht.
	
	public static double fahrkartenBezahlen(double x) {
	Scanner tastatur = new Scanner(System.in);
	double geld;
	
	System.out.println("\nDer Preis f�r die Tickets liegt bei: " + x + "�\n" + "Bitte bezahlen Sie den Betrag in 5 Cent bis 2� M�nzen");
    while(x > 0.0)
    {
    	geld = tastatur.nextDouble();
    	x = x - geld;
    	if(x > 0.0)
    	{
    		System.out.println("\nSie haben " + geld + "� eingeworfen\n" + "Es fehlen noch " + x + "�");
    	}
    }
    if(x < 0.0)
    {
    	x = (x * -1);
    	System.out.println("Ihnen werden " + x + "� ausgegeben");
    }
    
    return x;
	}
	
//-----------------------------------------------------------------------------------------
// Code der den Kunden informiert, dass das Ticket, bzw. die Tickets, gedruckt werden.
	
	public static void fahrkartenAusgeben(double x) {
	
	if (x <= 1)
	{
		System.out.println("\nFahrschein wird gedruckt");
	}
	else 
	{
		System.out.printf("\n"+ "%.0f " + " Fahrscheine werden gedruckt\n\n" , x);   
	}
	for (int i = 0; i < 8; i++)
	{
		System.out.print("=");
	    try {
	    	Thread.sleep(250);
			} 
	    catch (InterruptedException e) {
			e.printStackTrace();
			}
	}
	}
	
//-----------------------------------------------------------------------------------------
// Code der das R�ckkeld ausgibt und den Kauf abschlie�t
	
	public static void rueckgeldAusgeben(double x, double y) {
	
    if(x > 0.0)
    {
 	   System.out.printf("\n\nDer R�ckgabebetrag in H�he von" + " %.2f EURO ", (x));
 	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

        while(x >= 2.0) // 2 EURO-M�nzen
        {
     	  System.out.println("2 EURO");
	          x -= 2.0;
        }
        while(x >= 1.0) // 1 EURO-M�nzen
        {
     	  System.out.println("1 EURO");
	          x -= 1.0;
        }
        while(x >= 0.5) // 50 CENT-M�nzen
        {
     	  System.out.println("50 CENT");
	          x -= 0.5;
        }
        while(x >= 0.2) // 20 CENT-M�nzen
        {
     	  System.out.println("20 CENT");
	          x -= 0.2;
        }
        while(x >= 0.1) // 10 CENT-M�nzen
        {
     	  System.out.println("10 CENT");
	          x -= 0.1;
        }
        while(x >= 0.05)// 5 CENT-M�nzen
        {
     	  System.out.println("5 CENT");
	          x -= 0.05;
        }
    }
    if (y <= 1)
    {
 	   System.out.println("\nVergessen Sie nicht, den Fahrschein");
    }
    else 
    {
 	   System.out.println("\nVergessen Sie nicht, die Fahrscheine");
    }
    System.out.println("vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir w�nschen Ihnen eine gute Fahrt.");
    System.out.println();
 }
}