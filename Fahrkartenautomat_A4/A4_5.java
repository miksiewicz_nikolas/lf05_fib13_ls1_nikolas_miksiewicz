import java.util.Scanner;

public class A4_5{
	
	public static void main(String[] args) {	
	double groesse;
	double gewicht;
		
	groesse = groessenabfrage();
	gewicht = gewichtabrage();
	bmirechner(gewicht, groesse);
	}
	
//-----------------------------------------------------------------------------------------
// Code der die Groesse der Person ermittelt.
				
	public static double groessenabfrage() {
	Scanner tastatur = new Scanner(System.in);
	double groesse;
		
	System.out.println("Wie gross sind Sie in m?");
	groesse = tastatur.nextDouble();
	if (groesse >= 2.5) {
		System.out.println("Geben Sie bitte Ihre K�rpergr��e in Metern und nicht in cm an.");
		groesse = tastatur.nextDouble();
		}
	
	return groesse;
	}

//-----------------------------------------------------------------------------------------
// Code der das Gewicht der Person ermittelt.
		
	public static double gewichtabrage() {
	Scanner tastatur = new Scanner(System.in);
	double gewicht;
			
	System.out.println("Wie viel wiegen Sie in kilo?");
	gewicht = tastatur.nextDouble();
			
	return gewicht;
	}
	
//-----------------------------------------------------------------------------------------
// Code der das Geschlecht ermittelt und den BMI entsprechend berechnet.

	public static void bmirechner(double x, double y) {
	Scanner scan = new Scanner(System.in);
	double bmi = x / (y*y);
	String geschlecht;
	
	System.out.println("Sind Sie maennlich oder weiblich?");
	System.out.println("antworten Sie bitte mit einem m oder w.");
	geschlecht = scan.next();
		    
	if (!(geschlecht.equals("m") || geschlecht.equals("w"))) {
		System.out.println("Geben Sie bitte Ihr Geschelecht mit einem m oder w an.");
		geschlecht = scan.next();
	}
	
	System.out.println("Ihr BMI betr�gt: " + bmi);
	
	if (geschlecht.equals("m")) {
		if (bmi <= 20) {
			System.out.println("Sie sind untergewichtig");
		}
		if (bmi >= 25) {
			System.out.println("Sie sind untergewichtig");
		}
		else {
			System.out.println("Sie sind normalgewichtig");
		}
	}
	if (geschlecht.equals("w")) {
		if (bmi <= 19) {
			System.out.println("Sie sind untergewichtig");
		}
		if (bmi >= 24) {
			System.out.println("Sie sind untergewichtig");
		}
		else {
			System.out.println("Sie sind normalgewichtig");
		}
	}	
	}
}
