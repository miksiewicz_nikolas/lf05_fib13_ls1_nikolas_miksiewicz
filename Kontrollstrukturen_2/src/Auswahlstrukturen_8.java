import java.util.Scanner;

	public class Auswahlstrukturen_8 {

		public static void main(String[] args) {
		int jahr;
		
		jahr = jahresabfrage();
		schaltjahresrechner(jahr);
		}

		//-----------------------------------------------------------------------------------------
		// Code der das Jahr bestimmt
				
		public static int jahresabfrage() {
		Scanner tastatur = new Scanner(System.in);
		int jahr;
					
		System.out.println("Welches Jahr soll �berpr�ft werden, ob es ein Schaltjahr ist?");
		jahr = tastatur.nextInt();
					
		return jahr;
		}	
		
		//-----------------------------------------------------------------------------------------
		// Code der bestimmt ob das Jahr ein Schaltjahr ist
					
		public static void schaltjahresrechner(int x) {
			
		if (x % 400 == 0) {
			System.out.println("Es ist ein Schaltjahr");
			System.exit(x);
		}
		if (x % 4 == 0 && !(x % 100 == 0)) {
				System.out.println("Es ist ein Schaltjahr");
		}
		else {
			System.out.println("Es ist kein Schaltjahr");
		}
		}
	}
