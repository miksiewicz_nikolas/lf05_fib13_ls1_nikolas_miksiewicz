import java.util.Scanner;

	public class Fallunterscheidungen_4 {

		public static void main(String[] args) {
		
		double ergebnis = rechenweg();
		System.out.println("Ihr Ergebnis lautet: " + ergebnis);
		}
		
	//-----------------------------------------------------------------------------------------
	// Code der die Rechnung ausf�hrt
			
		public static double rechenweg() {
		double ergebnis = 0;
		int i = 1;
		
		double zahl1 = zahlen(i);
		i++;
		String operator = operatoren();
		double zahl2 = zahlen(i);
		
		if (operator.equals("+")) {
			ergebnis = zahl1 + zahl2;
		}
		if (operator.equals("-")) {
			ergebnis = zahl1 - zahl2;
		}
		if (operator.equals("*")) {
			ergebnis = zahl1 * zahl2;
		}
		if (operator.equals("/")) {
			ergebnis = zahl1 / zahl2;
		}
		
		return ergebnis;
		}
		
	//-----------------------------------------------------------------------------------------
	// Code der die Zahlen bestimmt
					
		public static double zahlen(int i) {
		Scanner scan = new Scanner(System.in);
		String zaehler = "Erste";
		
		if (i > 1) {
			zaehler = "Zweite";
		}

		System.out.println("Geben Sie bitte die " + zaehler + " Zahl ein");
		double zahl = scan.nextDouble();
		
		return zahl;
		}

	//-----------------------------------------------------------------------------------------
	// Code der den Rechenoperator bestimmt
		
		public static String operatoren() {
		Scanner scan = new Scanner(System.in);
		String operator = "";

		while (!(operator.equals("+") || operator.equals("-") || operator.equals("*") || operator.equals("/"))) {
			System.out.println("Geben Sie bitte einen der folgenden Rechenbefehle ein");
			System.out.println("+ , - , * , / ");
			operator = scan.next();
		}
		
		return operator;
		}
		
	}