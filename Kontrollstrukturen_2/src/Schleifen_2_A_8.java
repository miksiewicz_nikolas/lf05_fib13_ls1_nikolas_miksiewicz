import java.util.Scanner;

public class Schleifen_2_A_8 {

	public static void main(String[] args) {
	
	int zahl = eingabe();
	matrix(zahl);
	}
	
	//-----------------------------------------------------------------------------------------
	// Code der die Eingabe festlegt
	
	public static int eingabe() {
	Scanner scan = new Scanner(System.in);
	
	System.out.println("Dies ist ein Matrix Rechner. Geben Sie bitte eine Zahl zwischen 2 und 9 ein.");
	int zahl = scan.nextInt();
	
	while (zahl <= 1 || zahl >= 10) {
		System.out.println("Geben Sie bitte lediglich eine Zahl zwischen 2 und 9 ein.");
		zahl = scan.nextInt();
	}
	
	return zahl;
	}

	//-----------------------------------------------------------------------------------------
	// Code der die Berechnungen ausf�hrt und die Matrix erstellt

	public static void matrix(int x) {
	int i = 0;
	int u = 0;
	
	
	while(u < 100) {												// f�hrt aus: bis 10 Spalten erstellt worden sind (10 x 10)
		if(i % x == 0 && !(i == 0) || (i - u) == x) {				// pr�ft: angereihte Zahl durch die gew�hlte Zahl teilbar, nicht 0, enth�lt angereihte Zahl die ausgew�hlte Zahl?
			if(!((x * 10) % i == 1)) {								// pr�ft: nicht das 10, 20, ... fache
				System.out.print("*  ");
			}		
		}															

		else {
			if(i < 10) {											// pr�ft: angereihte Zahl �ber 10?
				System.out.print(i + "  ");
			}
			else {
				System.out.print(i + " ");	
			}
		}
		
		i ++;
		if(i % 10 == 0) {
			System.out.println();
			u = u + 10;
		}
	}
	
	}
}
