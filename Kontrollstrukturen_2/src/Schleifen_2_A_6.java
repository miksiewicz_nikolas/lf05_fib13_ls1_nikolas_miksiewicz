import java.util.Scanner;

public class Schleifen_2_A_6 {

	public static void main(String[] args) {
	Scanner scan = new Scanner(System.in);
	String frage = "j";
		
	while(frage.equals("j")) {
	double einlage = einlagen();
	double zins = zinssatz();
	berechnung(einlage, zins);
	System.out.println("Wollen Sie eine weitere Berechnung ausführen?");
	frage = scan.next();
	System.out.println();
	}
	}

	//-----------------------------------------------------------------------------------------
	// Code der die Einlage bestimmt
	
	public static double einlagen() {
	Scanner scan = new Scanner(System.in);
	
	System.out.println("Geben Sie bitte Ihre gewünschte Einlage in Euro ein.");	
	double einlage = scan.nextDouble();
	System.out.println();
	
	return einlage;
	}
	
	//-----------------------------------------------------------------------------------------
	// Code der den Zins bestimmt
	
	public static double zinssatz() {
	Scanner scan = new Scanner(System.in);
	
	System.out.println("Geben Sie nun bitte Ihren gewünschten Zinssatz in prozent ein.");	
	double zins = scan.nextDouble();
	
	zins = zins / 100;
	System.out.println();
	
	return zins;
	}
	
	//-----------------------------------------------------------------------------------------
	// Code der die Berechnung ausführt
	
	public static void berechnung(double x, double y) {
	double ergebnis = x;
	int jahre = 0;
	
	while(ergebnis <= 1000000) {
		ergebnis = ergebnis + (x*y);
		jahre ++;
	}

	System.out.println("Sie erreichen eine million Euro in " + jahre + " Jahren!");
	System.out.println();
	}
}
