import java.util.Scanner;

public class A4_5 {

	public static void main(String[] args) {
	
	String variable = variablebestimmen();
	berechnung(variable);
	}
	
//-----------------------------------------------------------------------------------------
// Code der die Variable bestimmt
	
	public static String variablebestimmen() {
	Scanner scan = new Scanner(System.in);
	String variable = "";
	
	System.out.println("welche Gr��e wollen Sie berechnen?");
	while (!(variable.equals("R") || variable.equals("r") || variable.equals("U") || variable.equals("u") || variable.equals("I") || variable.equals("i"))) {
		System.out.println("w�hlen Sie bitte zwischen R, U oder I");
		variable = scan.next();
	}
	return variable;
	}

//-----------------------------------------------------------------------------------------
// Code der die restilichen Variablen bestimmt und berechnet
	
	public static void berechnung(String variable) {
	Scanner scan = new Scanner(System.in);
	double ergebnis;
	
	if (variable.equals("R") || variable.equals("r")) {
		System.out.println("Geben Sie nun bitte die Werte f�r U und I ein");
		double u = scan.nextDouble();
		double i = scan.nextDouble();
		ergebnis = u/i;
		System.out.println("R = " + u + " / " + i);
		System.out.println("R = " + ergebnis + " Ohm");
	}
	if (variable.equals("U") || variable.equals("u")) {
		System.out.println("Geben Sie nun bitte die Werte f�r R und I ein");
		double r = scan.nextDouble();
		double i = scan.nextDouble();
		ergebnis = r*i;
		System.out.println("U = " + r + " * " + i);
		System.out.println("U = " + ergebnis + " Volt");
	}
	if (variable.equals("I") || variable.equals("i")) {
		System.out.println("Geben Sie nun bitte die Werte f�r U und R ein");
		double u = scan.nextDouble();
		double r = scan.nextDouble();
		ergebnis = u/r;
		System.out.println("I = " + u + " / " + r);
		System.out.println("I = " + ergebnis + " Ampere");
	}
	}

}
