import java.util.Scanner;

public class Fahrkartenarray 
{	
	public static void main(String[] args) {
	Scanner scan = new Scanner(System.in);
	String frage = "j";
	
	while (frage.equals("j") || frage.equals("ja") || frage.equals("klaro")){
		int art = ticketArt();
		int anzahl = ticketAnzahl(art);
		double kosten = zuZahlenderBetrag(art, anzahl);
		fahrkartenAusgeben(anzahl);
		rueckgeldAusgeben(kosten, anzahl);
		System.out.println("Wollen Sie einen weiteren Kauf durchf�hren?");
		frage = scan.next();
		System.out.println();
	}
	System.out.println("Auf wiedersehen!");
	scan.close();
	}

//-----------------------------------------------------------------------------------------
// Code der die Art des Tickets bestimmt
		
	public static int ticketArt() {
	Scanner scan = new Scanner(System.in);
	int art;
	String ticket[] = {"Einzelfahrschein", "Kurzstrecke", "4-Fahrten-Karte", "Anschlussfahrausweis", "4-Fahrten-Karte Kurzstrecke"} ;
	double[] preise = {3, 2, 9.4, 1.8, 6};


	System.out.println("Willkommen am BVG Fahrscheinautomat\n");
	
	while(true) {
		System.out.println("Welches Ticket wollen Sie kaufen?");
		for(int i = 0; i <= (ticket.length -1); i++){
			System.out.println((i+1) + "	" + ticket[i] + "		:" + preise[i] + " Euro");
		}
		
		System.out.println();
		art = scan.nextInt();
		if(art >= 1 && art <= ticket.length){
			break;
		}
		System.out.println("Bitte w�hlen Sie eine g�ltige Fahrscheinnummer aus!\n");
	}
	
	return art;
	}
	
//-----------------------------------------------------------------------------------------
// Code der die Anzahl der Tickets bestimmt
	
	public static int ticketAnzahl(int x) {
	Scanner scan = new Scanner(System.in);

	System.out.println("\nWie viele der gewaehlten Ticket mit der Nummer " + x + " wollen Sie kaufen?");
	int anzahl = scan.nextInt();
	
	return anzahl;
	}
//-----------------------------------------------------------------------------------------
// Code der sich um das Bezahlen des Tickets k�mmert und berechnet ob und wieviel R�ckgeld dem Kunden zusteht.
	
	public static double zuZahlenderBetrag(int art, int anzahl) {
	Scanner scan = new Scanner(System.in);
	art--;
	double[] preise = {3, 2, 9.4, 1.8, 6};
	double kosten = preise[art] * anzahl;
	double geld = 0;
	
	
	System.out.println("\nIhre Kaufsumme betraegt " + kosten + " Euro.");
	System.out.println("\nWerfen Sie bitte nun M�nzen zum bezahlen ein.");
	
    while(kosten > 0)
    {
    	geld = scan.nextDouble();
    	while(geld > 2) {
    		System.out.println("Werfen Sie bitte nur M�nzen zum bezahlen ein!");
    		geld = scan.nextDouble();
    	}
    	kosten = kosten - geld;
        if(kosten > 0) {
    	System.out.println("\nSie haben " + geld + " Euro eingeworfen.\n" + "Es fehlen noch " + kosten + " Euro.");
        }
    }
    
    if(kosten < 0)
    {
    	kosten = kosten * -1;
    	System.out.println("\nIhnen werden " + kosten + " Euro ausgegeben.");
    }
	
    return kosten;
	}
	
//-----------------------------------------------------------------------------------------
// Code der den Kunden informiert, dass das Ticket, bzw. die Tickets, gedruckt werden.
	
	public static void fahrkartenAusgeben(int x) {
	
	if (x <= 1)
	{
		System.out.println("\nFahrschein wird gedruckt.\n");
	}
	else 
	{
		System.out.println("\n" + x + " Fahrscheine werden gedruckt.\n");   
	}
	for (int i = 0; i < 8; i++)
	{
		System.out.print("=");
	    try {
	    	Thread.sleep(250);
			} 
	    catch (InterruptedException e) {
			e.printStackTrace();
			}
	}
	System.out.println();
	}
	
//-----------------------------------------------------------------------------------------
// Code der das R�ckkeld ausgibt und den Kauf abschlie�t
	
	public static void rueckgeldAusgeben(double x, int y) {
	
    if(x > 0.0)
    {
 	   System.out.printf("\n\nDer R�ckgabebetrag in H�he von" + " %.2f EURO ", (x));
 	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

        while(x >= 1.99) // 2 EURO-M�nzen
        {
     	  System.out.println("2 EURO");
	          x -= 2.0;
        }
        while(x >= 0.99) // 1 EURO-M�nzen
        {
     	  System.out.println("1 EURO");
	          x -= 1.0;
        }
        while(x >= 0.49) // 50 CENT-M�nzen
        {
     	  System.out.println("50 CENT");
	          x -= 0.5;
        }
        while(x >= 0.19) // 20 CENT-M�nzen
        {
     	  System.out.println("20 CENT");
	          x -= 0.2;
        }
        while(x >= 0.09) // 10 CENT-M�nzen
        {
     	  System.out.println("10 CENT");
	          x -= 0.1;
        }
        while(x >= 0.04)// 5 CENT-M�nzen
        {
     	  System.out.println("5 CENT");
	          x -= 0.05;
        }
        while(x >= 0.009)// 5 CENT-M�nzen
        {
     	  System.out.println("1 CENT");
	          x -= 0.01;
        }
    }
    if (y <= 1)
    {
 	   System.out.println("\nVergessen Sie nicht, den Fahrschein");
    }
    else 
    {
 	   System.out.println("\nVergessen Sie nicht, die Fahrscheine");
    }
    System.out.println("vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir w�nschen Ihnen eine gute Fahrt.");
    System.out.println();
 }
}